<?php
require("connect.php");
function connect_bd(){
	// pour oracle: $dsn="oci:dbname=//serveur:1521/mydb"
	$dsn="mysql:dbname=".BASE.";host=".SERVER;
		try{
		  $connexion=new PDO($dsn,USER,PASSWD);
		  $connexion->exec("SET CHARACTER SET utf8");
		}
		catch(PDOException $e){
		  printf("Échec de la connexion : %s\n", $e->getMessage());
		  exit();
		}
	 return $connexion;
	 }
?>

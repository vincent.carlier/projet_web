<?php
	require_once('connexion.php');
	$connexion=connect_bd();
	$errorMessage = '';
	// Les 2 identifiants sont-ils transmis ?
	if(!empty($_POST['login']) && !empty($_POST['password']))
	{
		 // Est-ce que l'utilisateur est dans la base ?
		 $sql="SELECT password FROM USER where login=:login";	
		 $stmt=$connexion->prepare($sql);
		 $stmt->bindParam(':login', $_POST['login']);
		 $stmt->execute();
		 $pass=md5($_POST['password']);
		if($stmt->rowCount() > 0){
		   $row=$stmt->fetch();
		   if ($row['password']==$pass){
			 // On ouvre la session
			 session_start();
			 // On enregistre le login en variable de session
			 $_SESSION['login'] = $_POST['login'];
			 echo "Bravo ".$_POST['login'];
			 // On redirige vers le fichier suite.php
			  header('Location: suite.php');
			}
			else $errorMessage = ' Mauvais identifiants !';
		 }
		else $errorMessage = "Utilisateur inconnu !";
	}
	 else $errorMessage = 'Veuillez inscrire vos 2 identifiants svp !';
?>


<!DOCTYPE html>
<html>
  <head>
  <meta charset="utf-8">
    <link href="bootstrap/css/bootstrap.css" rel="stylesheet">
    <link href="contenu/commun.css" rel="stylesheet">
    <title>Xtrem Sports - Login</title>
  </head>
  <body>
    <div class="container">
		<header class="row">
			<div class="col-sm-12">
				<img src="contenu/images/logo.png" alt="logo">
				<h1>L'Agenda Du Sportif</h1>
			</div>
		</header>
      <div class="row">
        <nav class="col-lg-offset-3 col-lg-6 col-lg-offset-3" id="menu">
			<a class="btn btn-primary btn-lg" role="button" href="index.php">Accueil</a>
			<a class="btn btn-primary btn-lg" role="button" href="listeActivites.php">Nos activités</a>
			<a class="btn btn-primary btn-lg" role="button" href="authentBD_Secure.php">Se connecter</a>
			<a class="btn btn-primary btn-lg" role="button" href="addPers.html">S'enregistrer</a>
        </nav>
        <section class="col-lg-offset-4 col-lg-4 col-lg-offset-4">
			<form name="f1" action="authentBD_Secure.php" method="POST">
				</br>
				</br>
				</br>
				<fieldset>
					<legend>Identifiez-vous</legend>
					<p>
					<label for="login" id="authen">Login :</label><input type="text" name="login" value=""/>
					</p>
					<p>
					<label for="password" id="authen">Password:</label><input type="password" name="password" value=""/>
					</br>
					</br>
					</br>
					<input type="submit" class="btn btn-warning" value="Login" id="login" />
				</fieldset>
			</form>
        </section>
      </div>
      <footer class="row">
		  <div class="col-lg-12">
		<a href="http://www.facebook.com" target="_blank" id="reseaux"><img src="contenu/images/facebook.png" alt="facebook"></a>
		<a href="http://www.twitter.com" target="_blank" id="reseaux"><img src="contenu/images/twitter.png" alt="twitter"></a>
		<a href="http://www.linkedin.com" target="_blank" id="reseaux"><img src="contenu/images/linkedin.png" alt="linkedin"></a>
		<a class="btn btn-success" data-toggle="button" onclick="self.location.href='mailto:vincent.carlier@etu.univ-orleans.fr'" id="contacts">Contacter Vincent</a>
		<a class="btn btn-success" data-toggle="button" onclick="self.location.href='mailto:leandre.chavanne@etu.univ-orleans.fr'" id="contacts">Contacter Léandre</a>
		</div>
      </footer>
    </div>
  </body>
</html>

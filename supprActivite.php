<!DOCTYPE html>
<html>
  <head>
  <meta charset="utf-8">
    <link href="bootstrap/css/bootstrap.css" rel="stylesheet">
    <link href="contenu/commun.css" rel="stylesheet">
    <title>Xtrem Sports - Votre agenda</title>
  </head>
  <body>
    <div class="container">
		<header class="row">
			<div class="col-sm-12">
				<img src="contenu/images/logo.png" alt="logo">
				<h1>L'Agenda Du Sportif</h1>
			</div>
		</header>
      <div class="row">
        <nav class="col-lg-offset-3 col-lg-6 col-lg-offset-3" id="menu">
			<a class="btn btn-primary btn-lg" role="button" href="index.php">Accueil</a>
			<a class="btn btn-primary btn-lg" role="button" href="listeActivites.php">Nos activités</a>
			<a class="btn btn-primary btn-lg" role="button" href="authentBD_Secure.php">Se connecter</a>
			<a class="btn btn-primary btn-lg" role="button" href="addPers.html">S'enregistrer</a>
        </nav>
        <section class="col-lg-offset-2 col-lg-8 col-lg-offset-2">
				<?php
					session_start();
					require("connexion.php");
					$connexion=connect_bd();
					$sql="DELETE from PLANIFIER where login=:login and DATE_BOOKING=:date and HEURE_BOOKING=:time;";
					$stmt=$connexion->prepare($sql);
					$stmt->bindParam(':date',$_SESSION['date']);
					$stmt->bindParam(':login', $_SESSION['login']);
					$stmt->bindParam(':time', $_GET['choix_time']);
					$stmt->execute();
					$row=$stmt->fetch();
					header('refresh: 2; booking.php?'.'date='.$_SESSION['date'].'');
				?>
        </section>
      </div>
      <footer class="row">
      	<div class="col-lg-12">
			<a href="http://www.facebook.com" target="_blank" id="reseaux"><img src="contenu/images/facebook.png" alt="facebook"></a>
			<a href="http://www.twitter.com" target="_blank" id="reseaux"><img src="contenu/images/twitter.png" alt="twitter"></a>
			<a href="http://www.linkedin.com" target="_blank" id="reseaux"><img src="contenu/images/linkedin.png" alt="linkedin"></a>
			<a class="btn btn-success" data-toggle="button" onclick="self.location.href='mailto:vincent.carlier@etu.univ-orleans.fr'" id="contacts">Contacter Vincent</a>
			<a class="btn btn-success" data-toggle="button" onclick="self.location.href='mailto:leandre.chavanne@etu.univ-orleans.fr'" id="contacts">Contacter Léandre</a>
		</div>
      </footer>
    </div>
  </body>
</html>
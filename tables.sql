-- projet_web


drop table USER;
drop table ACTIVITE;
drop table PLANIFIER;

create table USER(
login varchar(40) primary key, 
password varchar(40),  
email varchar(40)
);

create table ACTIVITE(
ID int(2) primary key,
LIBELLE varchar(40)
);

create table PLANIFIER(
login varchar(40),
ID int(2),
DATE_BOOKING date,
HEURE_BOOKING time,
primary key(login,DATE_BOOKING, HEURE_BOOKING),
foreign key (login) references USER(login) on delete cascade,
foreign key (ID) references ACTIVITE(ID) on delete cascade
);

<!DOCTYPE html>
<html>
  <head>
  <meta charset="utf-8">
    <link href="bootstrap/css/bootstrap.css" rel="stylesheet">
    <link href="contenu/commun.css" rel="stylesheet">
    <title>Xtrem Sports - Accueil</title>

  </head>
  <body>
    <div class="container">
		<header class="row">
			<div class="col-sm-12">
				<img src="contenu/images/logo.png" alt="logo">
				<h1>L'Agenda Du Sportif</h1>
			</div>
		</header>
      <div class="row">
        <nav class="col-lg-offset-3 col-lg-6 col-lg-offset-3" id="menu">
			<a class="btn btn-primary btn-lg" role="button" href="index.php">Accueil</a>
			<a class="btn btn-primary btn-lg" role="button" href="listeActivites.php">Nos activités</a>
			<a class="btn btn-primary btn-lg" role="button" href="authentBD_Secure.php">Se connecter</a>
			<a class="btn btn-primary btn-lg" role="button" href="addPers.html">S'enregistrer</a>
        </nav>
        <section class="col-lg-offset-2 col-lg-8 col-lg-offset-2">
			<iframe id="video" src="//www.youtube.com/embed/SwbP9WLX3fY" frameborder="0" allowfullscreen=""></iframe>
          <div class="row">
			<br>
			<br>
			<br>
            <article class="col-md-9">
				<img src="contenu/images/MFourcade.jpg" alt="PhotoArticle" id="PhotoArticle">
				<font size=2.5>INTERVIEW du parrain de ce superbe site! – Le vainqueur français de la Coupe du monde aborde cette saison olympique avec appétit…
				Après un été de travail intense, parfois en skis à roulettes, Martin Fourcade retrouvera le chemin de la Coupe du monde dimanche, à Östersund, en Suède. La première course d’un hiver dont le sommet est attendu au mois de février lors des JO de Sotchi. L’objectif prioritaire de la saison du Français…
				<a class="btn btn-link" href="http://www.20minutes.fr/sport/1253651-20131123-martin-fourcade-cest-peut-etre-lache-tant-sportif-faut-rester-a-place">lire la suite</a>
				<font>
            </article>
            <aside class="col-md-3">
			<h5>News de la semaine:</h5>
				<ul>
					<li>Edition du site</li>
					<li>Mise en place du site</li>
					<li>Bienvenue au adhérents</li>
					<li>Forum a venir</li>
					<li>Section tutoriel entrainement</li>
					<li>Recrutements admin</li>
				</ul>
            </aside>
          </div>
        </section>
      </div>
      <footer class="row">
		  <div class="col-lg-12">
		<a href="http://www.facebook.com" target="_blank" id="reseaux"><img src="contenu/images/facebook.png" alt="facebook"></a>
		<a href="http://www.twitter.com" target="_blank" id="reseaux"><img src="contenu/images/twitter.png" alt="twitter"></a>
		<a href="http://www.linkedin.com" target="_blank" id="reseaux"><img src="contenu/images/linkedin.png" alt="linkedin"></a>
		<a class="btn btn-success" data-toggle="button" onclick="self.location.href='mailto:vincent.carlier@etu.univ-orleans.fr'" id="contacts">Contacter Vincent</a>
		<a class="btn btn-success" data-toggle="button" onclick="self.location.href='mailto:leandre.chavanne@etu.univ-orleans.fr'" id="contacts">Contacter Léandre</a>
		</div>
      </footer>
    </div>
  </body>
</html>

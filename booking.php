<!DOCTYPE html>
<?php
session_start();
?>
<html>
  <head>
  <meta charset="utf-8">
    <link href="bootstrap/css/bootstrap.css" rel="stylesheet">
    <link href="contenu/commun.css" rel="stylesheet">
    <title>Xtrem Sports - Votre agenda</title>

  </head>
  <body>
    <div class="container">
		<header class="row">
			<article id='deco'>
				<?php
				if(!empty($_SESSION['login']))
					echo "Vous êtes connecté ".$_SESSION['login']."   ";
					echo "<input class='btn btn-danger' type='button' onclick=\"location.href='deconnect.php'\" value='Déconnection'/>";
				?>
			</article>
			<div class="col-sm-12">
				<img src="contenu/images/logo.png" alt="logo">
				<h1>L'Agenda Du Sportif</h1>
			</div>
		</header>
      <div class="row">
        <nav class="col-lg-offset-3 col-lg-6 col-lg-offset-3" id="menu">
			<a class="btn btn-primary btn-lg" role="button" href="index.php">Accueil</a>
			<a class="btn btn-primary btn-lg" role="button" href="listeActivites.php">Nos activités</a>
			<a class="btn btn-primary btn-lg" role="button" href="authentBD_Secure.php">Se connecter</a>
			<a class="btn btn-primary btn-lg" role="button" href="addPers.html">S'enregistrer</a>
        </nav>
        <section class="col-lg-offset-2 col-lg-8 col-lg-offset-2">
			<article id='page'>
				<?php
					if (!empty($_SESSION['login'])){
					echo "<h3>Bienvenue " .$_SESSION['login']."<br/>";
					echo 'Voici votre emploi du temps pour la journée du '.$_GET['date'].'</h2>';
					$_SESSION['date']=date("Y-m-d", strtotime($_GET['date']));
					$_SESSION['datepick']=$_GET['date'];
					require_once("connexion.php");
					$connexion=connect_bd();
					}
					else header('Location: authentBD_Secure.php');
				?>
				<br>
				<table>
						<tr>
							<th>HORAIRE</th>
							<th>ACTIVITE</th>
						</tr>
						<tr><td>8h
							</td>
							<td><?php
								$sql="SELECT * from ACTIVITE natural join PLANIFIER where DATE_BOOKING=:date and HEURE_BOOKING='08:00:00.000000';";
								$stmt=$connexion->prepare($sql);
								$stmt->bindParam(':date',$_SESSION['date']);
								$stmt->execute();
								$row=$stmt->fetch();
								echo $row['LIBELLE'];
								?>	
							</td>
						</tr>
						<tr><td>9h
							</td>
							<td><?php
								$sql="SELECT * from ACTIVITE natural join PLANIFIER where DATE_BOOKING=:date and HEURE_BOOKING='09:00:00.000000';";
								$stmt=$connexion->prepare($sql);
								$stmt->bindParam(':date',$_SESSION['date']);
								$stmt->execute();
								$row=$stmt->fetch();
								echo $row['LIBELLE'];
								?>	
							</td>
						</tr>
						<tr><td>10h
							</td>
							<td><?php
								$sql="SELECT * from ACTIVITE natural join PLANIFIER where DATE_BOOKING=:date and HEURE_BOOKING='10:00:00.000000';";
								$stmt=$connexion->prepare($sql);
								$stmt->bindParam(':date',$_SESSION['date']);
								$stmt->execute();
								$row=$stmt->fetch();
								echo $row['LIBELLE'];
								?>		
							</td>
						</tr>
						<tr><td>11h
							</td>
							<td><?php
								$sql="SELECT * from ACTIVITE natural join PLANIFIER where DATE_BOOKING=:date and HEURE_BOOKING='11:00:00.000000';";
								$stmt=$connexion->prepare($sql);
								$stmt->bindParam(':date',$_SESSION['date']);
								$stmt->execute();
								$row=$stmt->fetch();
								echo $row['LIBELLE'];
								?>		
							</td>
						</tr>
						<tr><td>12h
							</td>
							<td><?php
								$sql="SELECT * from ACTIVITE natural join PLANIFIER where DATE_BOOKING=:date and HEURE_BOOKING='12:00:00.000000';";
								$stmt=$connexion->prepare($sql);
								$stmt->bindParam(':date',$_SESSION['date']);
								$stmt->execute();
								$row=$stmt->fetch();
								echo $row['LIBELLE'];
								?>		
							</td>
						</tr>
						<tr><td>13h
							</td>
							<td><?php
								$sql="SELECT * from ACTIVITE natural join PLANIFIER where DATE_BOOKING=:date and HEURE_BOOKING='13:00:00.000000';";
								$stmt=$connexion->prepare($sql);
								$stmt->bindParam(':date',$_SESSION['date']);
								$stmt->execute();
								$row=$stmt->fetch();
								echo $row['LIBELLE'];
								?>		
							</td>
						</tr>
						<tr><td>14h
							</td>
							<td><?php
								$sql="SELECT * from ACTIVITE natural join PLANIFIER where DATE_BOOKING=:date and HEURE_BOOKING='14:00:00.000000';";
								$stmt=$connexion->prepare($sql);
								$stmt->bindParam(':date',$_SESSION['date']);
								$stmt->execute();
								$row=$stmt->fetch();
								echo $row['LIBELLE'];
								?>		
							</td>
						</tr>
						<tr><td>15h
							</td>
							<td><?php
								$sql="SELECT * from ACTIVITE natural join PLANIFIER where DATE_BOOKING=:date and HEURE_BOOKING='15:00:00.000000';";
								$stmt=$connexion->prepare($sql);
								$stmt->bindParam(':date',$_SESSION['date']);
								$stmt->execute();
								$row=$stmt->fetch();
								echo $row['LIBELLE'];
								?>		
							</td>
						</tr>
						<tr><td>16h
							</td>
							<td><?php
								$sql="SELECT * from ACTIVITE natural join PLANIFIER where DATE_BOOKING=:date and HEURE_BOOKING='16:00:00.000000';";
								$stmt=$connexion->prepare($sql);
								$stmt->bindParam(':date',$_SESSION['date']);
								$stmt->execute();
								$row=$stmt->fetch();
								echo $row['LIBELLE'];
								?>		
							</td>
						</tr>
						<tr><td>17h
							</td>
							<td><?php
								$sql="SELECT * from ACTIVITE natural join PLANIFIER where DATE_BOOKING=:date and HEURE_BOOKING='17:00:00.000000';";
								$stmt=$connexion->prepare($sql);
								$stmt->bindParam(':date',$_SESSION['date']);
								$stmt->execute();
								$row=$stmt->fetch();
								echo $row['LIBELLE'];
								?>		
							</td>
						</tr>
						<tr><td>18h
							</td>
							<td><?php
								$sql="SELECT * from ACTIVITE natural join PLANIFIER where DATE_BOOKING=:date and HEURE_BOOKING='18:00:00.000000';";
								$stmt=$connexion->prepare($sql);
								$stmt->bindParam(':date',$_SESSION['date']);
								$stmt->execute();
								$row=$stmt->fetch();
								echo $row['LIBELLE'];
								?>		
							</td>
						</tr>
						<tr><td>19h
							</td>
							<td><?php
								$sql="SELECT * from ACTIVITE natural join PLANIFIER where DATE_BOOKING=:date and HEURE_BOOKING='19:00:00.000000';";
								$stmt=$connexion->prepare($sql);
								$stmt->bindParam(':date',$_SESSION['date']);
								$stmt->execute();
								$row=$stmt->fetch();
								echo $row['LIBELLE'];
								?>		
							</td>
						</tr>
					
				</table>
				<br>
				<form action="add_booking.php" method="GET" id="roulant_valider">
					
					<select name="choix_activite" class="form-control">
						<?php
							$sql2="SELECT * from ACTIVITE;";
							foreach ($connexion->query($sql2) as $row)
							if(!empty($row['LIBELLE']))
							echo "<option value=" .$row['ID']. ">".$row['LIBELLE']."</option>\n";
						?>
					</select>
					<br>
					<select name="choix_time" class="form-control">
						<option value="08:00:00.000000">de 8h à 9h
						<option value="09:00:00.000000">de 9h à 10h
						<option value="10:00:00.000000">de 10h à 11h
						<option value="11:00:00.000000">de 11h à 12h
						<option value="12:00:00.000000">de 12h à 13h
						<option value="13:00:00.000000">de 13h à 14h
						<option value="14:00:00.000000">de 14h à 15h
						<option value="15:00:00.000000">de 15h à 16h
						<option value="16:00:00.000000">de 16h à 17h
						<option value="17:00:00.000000">de 17h à 18h
						<option value="18:00:00.000000">de 18h à 19h
						<option value="19:00:00.000000">de 19h à 20h
					</select>
					
					<input class="btn btn-success" type="submit" value="Valider"/>
				</form>
				
				<form action="supprActivite.php" method="GET" id="roulant_supprimer">
				<br>
					<select name="choix_time" class="form-control">
						<option value="08:00:00.000000">de 8h à 9h
						<option value="09:00:00.000000">de 9h à 10h
						<option value="10:00:00.000000">de 10h à 11h
						<option value="11:00:00.000000">de 11h à 12h
						<option value="12:00:00.000000">de 12h à 13h
						<option value="13:00:00.000000">de 13h à 14h
						<option value="14:00:00.000000">de 14h à 15h
						<option value="15:00:00.000000">de 15h à 16h
						<option value="16:00:00.000000">de 16h à 17h
						<option value="17:00:00.000000">de 17h à 18h
						<option value="18:00:00.000000">de 18h à 19h
						<option value="19:00:00.000000">de 19h à 20h
					</select>
					
					<input type="submit" class="btn btn-danger" value="Supprimer"/>
					<br>
					<br>
					<input type='button' class="btn btn-warning" onclick="location.href='choixDate.php'" value='Autre date' id="choix_date"/>
				</form>
			</article>   
        </section>
      </div>
      <footer class="row">
		  <div class="col-lg-12">
		<a href="http://www.facebook.com" target="_blank" id="reseaux"><img src="contenu/images/facebook.png" alt="facebook"></a>
		<a href="http://www.twitter.com" target="_blank" id="reseaux"><img src="contenu/images/twitter.png" alt="twitter"></a>
		<a href="http://www.linkedin.com" target="_blank" id="reseaux"><img src="contenu/images/linkedin.png" alt="linkedin"></a>
		<a class="btn btn-success" data-toggle="button" onclick="self.location.href='mailto:vincent.carlier@etu.univ-orleans.fr'" id="contacts">Contacter Vincent</a>
		<a class="btn btn-success" data-toggle="button" onclick="self.location.href='mailto:leandre.chavanne@etu.univ-orleans.fr'" id="contacts">Contacter Léandre</a>
		</div>
      </footer>
    </div>
  </body>
</html>

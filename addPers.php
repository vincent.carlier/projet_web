<!DOCTYPE html>
<html>
  <head>
  <meta charset="utf-8">
    <link href="bootstrap/css/bootstrap.css" rel="stylesheet">
    <link href="contenu/commun.css" rel="stylesheet">
    <title>Xtrem Sports - Inscription</title>
  </head>
  <body>
    <div class="container">
		<header class="row">
			<div class="col-sm-12">
				<img src="contenu/images/logo.png" alt="logo">
				<h1>L'Agenda Du Sportif</h1>
			</div>
		</header>
      <div class="row">
        <nav class="col-lg-offset-3 col-lg-6 col-lg-offset-3" id="menu">
			<a class="btn btn-primary btn-lg" role="button" href="index.php">Accueil</a>
			<a class="btn btn-primary btn-lg" role="button" href="listeActivites.php">Nos activités</a>
			<a class="btn btn-primary btn-lg" role="button" href="authentBD_Secure.php">Se connecter</a>
			<a class="btn btn-primary btn-lg" role="button" href="addPers.html">S'enregistrer</a>
        </nav>
        <section class="col-lg-offset-2 col-lg-8 col-lg-offset-2">
			<?php
					require("connexion.php");
					$connexion=connect_bd();
					$sql="SELECT * from USER where login=:login;";
					$stmt=$connexion->prepare($sql);
					$stmt->bindParam(':login', $_GET['login']);
					$stmt->execute();
					if (!$stmt) die("pb d'accès à la table");
					else if ($stmt->rowCount()==0){		
						$sql="INSERT into USER values(:login,:password,:email)";
						$stmt=$connexion->prepare($sql);
						$stmt->bindParam(':login', $_GET['login']);
						$stmt->bindParam(':password', md5($_GET['password']));
						$stmt->bindParam(':email', $_GET['email']);
						$stmt->execute();
						if (!$stmt) echo "Pb d'insertion";
						else{
							echo "Inscription réussie, bienvenue ".$_GET['login'];
							header('Refresh: 2; authentBD_Secure.php');	
							}
					}
					else{
						echo "Incorrect";
						header('Refresh: 2; addPers.html');
					}
				?>
        </section>
      </div>
      <footer class="row">
		<div class="col-lg-12">
		<a href="http://www.facebook.com" target="_blank" id="reseaux"><img src="contenu/images/facebook.png" alt="facebook"></a>
		<a href="http://www.twitter.com" target="_blank" id="reseaux"><img src="contenu/images/twitter.png" alt="twitter"></a>
		<a href="http://www.linkedin.com" target="_blank" id="reseaux"><img src="contenu/images/linkedin.png" alt="linkedin"></a>
		<a class="btn btn-success" data-toggle="button" onclick="self.location.href='mailto:vincent.carlier@etu.univ-orleans.fr'" id="contacts">Contacter Vincent</a>
		<a class="btn btn-success" data-toggle="button" onclick="self.location.href='mailto:leandre.chavanne@etu.univ-orleans.fr'" id="contacts">Contacter Léandre</a>
		</div>
      </footer>
    </div>
  </body>
</html>

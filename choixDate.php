<!DOCTYPE html>
<?php
session_start();
?>
<html>
  <head>
  <meta charset="utf-8">
    <link href="bootstrap/css/bootstrap.css" rel="stylesheet">
    <link href="contenu/commun.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="contenu/Data_DateTimePicker/jquery.datetimepicker.css"/>
    <title>Xtrem Sports - Choix de la date</title>

  </head>
  <body>
    <div class="container">
		<header class="row">
			<article id='deco'>
				<?php
				if(!empty($_SESSION['login']))
					echo "Vous êtes connecté ".$_SESSION['login']."   ";
					echo "<input class='btn btn-danger'type='button' onclick=\"location.href='deconnect.php'\" value='Déconnection'/>";
				?>
			</article>
			<div class="col-sm-12">
				<img src="contenu/images/logo.png" alt="logo">
				<h1>L'Agenda Du Sportif</h1>
			</div>
		</header>
      <div class="row">
        <nav class="col-lg-offset-3 col-lg-6 col-lg-offset-3" id="menu">
			<a class="btn btn-primary btn-lg" role="button" href="index.php">Accueil</a>
			<a class="btn btn-primary btn-lg" role="button" href="listeActivites.php">Nos activités</a>
			<a class="btn btn-primary btn-lg" role="button" href="authentBD_Secure.php">Se connecter</a>
			<a class="btn btn-primary btn-lg" role="button" href="addPers.html">S'enregistrer</a>
        </nav>
        <section class="col-lg-offset-2 col-lg-8 col-lg-offset-2">
			<br>
			<br>
			<article id='datepicker'>
				<h3>Choix de la date</h3>
				<form name="ajout" action="booking.php" method="GET">
						<input name='date' type="text" id="datetimepicker2"/><br><br>
						<script src="contenu/Data_DateTimePicker/jquery.js"></script>
						<script src="contenu/Data_DateTimePicker/jquery.datetimepicker.js">
						</script>
						<script>
						$('#datetimepicker2').datetimepicker({
							lang:'fr',
							timepicker:false,
							format:"d.m.Y",
							minDate:'today',
						});
						</script>
						<p><input class="btn btn-success" type="submit" value="envoyer"></p>
				</form>
			</article>
          
        </section>
      </div>
      <footer class="row">
		  <div class="col-lg-12">
		<a href="http://www.facebook.com" target="_blank" id="reseaux"><img src="contenu/images/facebook.png" alt="facebook"></a>
		<a href="http://www.twitter.com" target="_blank" id="reseaux"><img src="contenu/images/twitter.png" alt="twitter"></a>
		<a href="http://www.linkedin.com" target="_blank" id="reseaux"><img src="contenu/images/linkedin.png" alt="linkedin"></a>
		<a class="btn btn-success" data-toggle="button" onclick="self.location.href='mailto:vincent.carlier@etu.univ-orleans.fr'" id="contacts">Contacter Vincent</a>
		<a class="btn btn-success" data-toggle="button" onclick="self.location.href='mailto:leandre.chavanne@etu.univ-orleans.fr'" id="contacts">Contacter Léandre</a>
		</div>
      </footer>
    </div>
  </body>
</html>
